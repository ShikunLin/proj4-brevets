"""
Author: Shikun Lin
CIS322 UO 18F 11/06/2018

This is a calculate implementation of open and close time
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow


#Holder for minimumSpeeds in differ range
minimumSpeeds = [15,15,15,11.428]

#Holder for maximumSpeeds in differ range
maximumSpeeds = [34,32,30,28]


# This is an helper function which take time in hours and a
#brevet_start_time in isoformat srtring as argumets. Then, update
#the iso time by added hours needed.
#After I finished this helper function, I think there have an
#esay way to do this by using shift() function which is built in
#arrow module
def add_time_and_convert(time_needs,brevet_start_time):

    #placeholder for day needed
    res_day = 0

    #if the hours needed bigger than 24. Then, update
    #the res_day and time_needs
    if time_needs >= 24:
        res_day = int(time_needs/24)
        time_needs = time_needs - res_day * 24


    #placeholder for hours needed
    res_hour = int(time_needs)

    #placeholder for minis needed
    res_mins = round((time_needs - res_hour) * 60)

    #A dict which contian the number of days in different month
    days_in_month = {'01': 31, '02': 28, '03': 31,'04': 30,'05':31,'06':30,
    '07':31,'08':31,'09':30,'10':31,'11':30,'12':31};

    #Holder for the years
    years = int(brevet_start_time[0:4])

    #If the years can be evenly divided by 4, then Feb have 29 days
    if years % 4 == 0:
        days_in_month['02'] = 29


    #Holder for hours in brevet_start_time
    origin_hour = int(brevet_start_time[11:13])

    #Holder for mins in brevet_start_time
    origin_mins = int(brevet_start_time[14:16])

    #Added time need to the orginal hours and mins
    res_hour = res_hour + origin_hour
    res_mins = res_mins + origin_mins

    #If the res_mins bigger than 60, which means we need update
    #the res_hour and res_mins
    if res_mins - 60 >= 0:
        res_mins = res_mins - 60
        res_hour = res_hour + 1

    #If the res_hour bigger than 24, which means we need update
    #the res_day and res_hour
    if res_hour - 24 > 0:
        res_day = res_day + 1
        res_hour = res_hour -1

    #Holder for day in brevet_start_time
    origin_day = int(brevet_start_time[8:10])
    #Holder for month in brevet_start_time
    origin_month = int(brevet_start_time[5:7])

    #Holder for string of month in brevet_start_time which used as
    # a key of dict
    month = brevet_start_time[5:7]

    #update the days, month and years
    origin_day = origin_day + res_day
    if origin_day > days_in_month[month]:
        origin_month = origin_month + 1
        origin_day = origin_day - days_in_month[month]
        if origin_month - 12 > 0:
            yesrs = yesrs + 1
            origin_month = origin_month - 12

    #update the iso time string
    res = list(brevet_start_time)
    res[0:4] = str(years)
    res[5:7] = '%0*d' % (2,origin_month)
    res[8:10] = '%0*d' % (2,origin_day)
    res[11:13] = '%0*d' % (2,res_hour)
    res[14:16] = '%0*d' % (2,res_mins)
    res = "".join(res)

    return res



#This function can take a control point distance in KM, brevet distance
#in KM and a brevet_start_time in iso time string to calculate the
#open time for the location
def open_time(control_dist_km, brevet_dist_km, brevet_start_time):

    #If the distance of control point locate is 20% bigger than
    #brevet distance. Then, return "outRange"
    if control_dist_km > (brevet_dist_km*1.2):
        return "outRange"

    #Holders for time needed for fixed location
    time_for_two = 200/maximumSpeeds[0]
    time_for_three = 100/maximumSpeeds[1] + time_for_two
    time_for_four = 200/maximumSpeeds[1]+time_for_two
    time_for_six = 200/maximumSpeeds[2]+time_for_four
    time_for_ten = 400/maximumSpeeds[3]+time_for_six

    #Holder for time needed
    time_needs = 0

    #Calculates the needed time based on the rules
    #described at https://rusa.org/octime_alg.html
    if control_dist_km <= 200 and control_dist_km > 0:
        time_needs = control_dist_km/maximumSpeeds[0]
    elif control_dist_km> 200 and control_dist_km <= 400:
        time_needs = (control_dist_km-200)/maximumSpeeds[1]+time_for_two
    elif control_dist_km > 400 and control_dist_km <=600:
        time_needs = (control_dist_km-400)/maximumSpeeds[2]+time_for_four
    elif control_dist_km > 600 and control_dist_km <=1000:
        time_needs = (control_dist_km-600)/maximumSpeeds[3]+time_for_six



    if control_dist_km > brevet_dist_km and control_dist_km <= (brevet_dist_km*1.2):
        if brevet_dist_km == 200:
            time_needs = time_for_two
        elif brevet_dist_km == 300:
            time_needs = time_for_three
        elif brevet_dist_km == 400:
            time_needs = time_for_four
        elif brevet_dist_km == 600:
            time_needs = time_for_six
        elif brevet_dist_km == 1000:
            time_needs = time_for_ten

    #update the result of iso time
    res = add_time_and_convert(time_needs,brevet_start_time)
    return res



#This function can take a control point distance in KM, brevet distance
#in KM and a brevet_start_time in iso time string to calculate the
#close time for the location
def close_time(control_dist_km, brevet_dist_km, brevet_start_time):

    #If the distance of control point locate is 20% bigger than
    #brevet distance. Then, return "outRange"
    if control_dist_km > (brevet_dist_km*1.2):
        return "outRange"

    #Holders for time needed for fixed location
    time_for_two = 200/minimumSpeeds[0]
    time_for_three = 100/maximumSpeeds[1] + time_for_two
    time_for_four = 200/minimumSpeeds[1]+time_for_two
    time_for_six = 200/minimumSpeeds[2]+time_for_four
    time_for_ten = 400/minimumSpeeds[3]+time_for_six

    #Holder for time needed
    time_needs = 0

    #Calculates the needed time based on the rules
    #described at https://rusa.org/octime_alg.html
    if control_dist_km == 0:
        time_needs = 1
    if control_dist_km == 50:
        time_needs = 3.5
    elif control_dist_km <= 200 and control_dist_km > 0:
        time_needs = control_dist_km/minimumSpeeds[0]
    elif control_dist_km> 200 and control_dist_km <= 400:
        time_needs = (control_dist_km-200)/minimumSpeeds[1]+time_for_two
    elif control_dist_km > 400 and control_dist_km <=600:
        time_needs = (control_dist_km-400)/minimumSpeeds[2]+time_for_four
    elif control_dist_km > 600 and control_dist_km <=1000:
        time_needs = (control_dist_km-600)/minimumSpeeds[3]+time_for_six

    if control_dist_km >= brevet_dist_km and control_dist_km <= (brevet_dist_km*1.2):
        if brevet_dist_km == 200:
            time_needs = 13.5
        elif brevet_dist_km == 300:
            time_needs = 20
        elif brevet_dist_km == 400:
            time_needs = 27
        elif brevet_dist_km == 600:
            time_needs = 40
        elif brevet_dist_km == 1000:
            time_needs = 75
    #update the result of iso time
    res = add_time_and_convert(time_needs,brevet_start_time)
    return res
