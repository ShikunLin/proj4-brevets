"""
Author: Shikun Lin
CIS322 UO 18F 11/06/2018

Nose test for close time caculation in acp_times.py

"""

from acp_times import *

import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)



def test_close_time():
    assert close_time(0, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T01:00:00-08:00"
    assert close_time(50, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T03:30:00-08:00"
    assert close_time(150, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T10:00:00-08:00"
    assert close_time(200, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T13:30:00-08:00"


def test_close_time2():
    assert close_time(200, 200,"2018-11-11T00:00:00-08:00")==close_time(210, 200,"2018-11-11T00:00:00-08:00")
    assert close_time(200, 200,"2018-11-11T00:00:00-08:00")!=close_time(280, 200,"2018-11-11T00:00:00-08:00")
