"""
Author: Shikun Lin
CIS322 UO 18F 11/06/2018

Nose test for open time caculation in acp_times.py

"""

from acp_times import *

import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)



def test_open_time():
    assert open_time(0, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T00:00:00-08:00"
    assert open_time(50, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T01:28:00-08:00"
    assert open_time(150, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T04:25:00-08:00"
    assert open_time(200, 200,"2018-11-11T00:00:00-08:00")=="2018-11-11T05:53:00-08:00"
    assert open_time(200, 200,"2018-11-11T00:00:00-08:00")==open_time(210, 200,"2018-11-11T00:00:00-08:00")
    assert open_time(200, 200,"2018-11-11T00:00:00-08:00")!=open_time(280, 200,"2018-11-11T00:00:00-08:00")

def test_open_time2():
    assert open_time(0, 400,"2018-11-11T00:00:00-08:00")=="2018-11-11T00:00:00-08:00"
    assert open_time(60, 400,"2018-11-11T00:00:00-08:00")=="2018-11-11T01:46:00-08:00"
    assert open_time(175, 400,"2018-11-11T00:00:00-08:00")=="2018-11-11T05:09:00-08:00"
    assert open_time(400, 400,"2018-11-11T00:00:00-08:00")=="2018-11-11T12:08:00-08:00"
    assert open_time(400, 400,"2018-11-11T00:00:00-08:00")==open_time(410, 400,"2018-11-11T00:00:00-08:00")
def test_open_time3():
    assert open_time(200, 200,"2018-11-11T02:00:00-08:00")!=open_time(200, 200,"2018-11-11T00:00:00-08:00")
