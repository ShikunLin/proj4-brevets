# Project 4: Brevet time calculator with Ajax
Author: Shikun Lin  
Contact address: shikunl@uoregon.edu  
## Description and Specification
This project is a simple ACP Brevet Control Times Calculator. This is a web program. User can input an begin date on web. Then, choose a brevet distance. Next, input control point distace, the open time and close time would show on the web. However, if the distance of control point is 20% larger than brevet distance, it will show a message with error. You can find some test cases in ./brevets/tests. If you want to use it for test, run "nosetests".  

## How to Run 
1. Open the terminal, then cd to the root directory which contain Dockfile which in ./brevets
2. Build the image with "docker build -t flask-demo"  
3. Run the container using "docker run -d -p 5000:5000 flask-demo"  
4. Launch  [http://127.0.0.1:5000](http://127.0.0.1:5000/)  using web browser